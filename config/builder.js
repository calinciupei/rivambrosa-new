const webpack = require('webpack');
const webpackConfig = require('../webpack.config');

const argv = require('yargs')
  .usage('node builder.js [--prod] [--brand <bradName>] [--channel <channel>] [--wba]')
  .boolean('prod')
  .option('brand')
  .option('channel')
  .boolean('wba')
  .boolean('stats')
  .default('stats', true)
  .default('wba')
  .default('wba', false)
  .boolean('bundle-buddy')
  .default('bundle-buddy', false).argv;

const buildOptions = {
  brand: argv.brand,
  channel: argv.channel,
  isProd: argv.prod,
  analyzeBundle: argv.wba,
  bundleBuddy: argv['bundle-buddy'],
};

// precess.env variable setup
process.env.NODE_ENV = argv.prod ? 'production' : 'development';

const configs = webpackConfig(buildOptions);

configs.forEach(({config, buildId}) => {
  webpack(config).run((_err, stats) => {
    if (argv.stats) {
      console.log((stats || '').toString({colors: true}));
    }

    console.log(`Build ${buildId} done.`);
  });
});

const path = require('path');
const webpack = require('webpack');
const webpackDevServer = require('webpack-dev-server');
const webpackConfig = require('./../webpack-bundle.config');
const argv = require('yargs')
  .usage('node builder.js [--brand <bradName>] [--channel <channel>]')
  .option('brand')
  .option('channel')
  .boolean('stats')
  .default('stats', true).argv;
const buildOptions = {
  brand: argv.brand,
  channel: argv.channel,
};
const HOST = {
  rivambrosa: 'localrivambrosa.com',
};
const compiler = webpack(webpackConfig(buildOptions));

const devServerOptions = Object.assign({}, webpackConfig.devServer, {
  contentBase: path.resolve(__dirname, '/'),
  disableHostCheck: true,
  historyApiFallback: true,
  https: false,
  hot: true,
  host: HOST[argv.brand],
  port: 3001,
  stats: {
    colors: true,
  },
});

const server = new webpackDevServer(compiler, devServerOptions);

server.listen(3001, '127.0.0.1');

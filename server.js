const express = require('express');
const compression = require('compression');
const PORT = process.env.PORT || 3001;

express()
  .use(compression())
  .use(express.static(`${__dirname}/dist/`))
  .get('/*', (req, res) => {
    res.sendFile(`${__dirname}/dist/index.html`);
  })
  .listen(PORT, () => console.log(`Listening on ${PORT}`));

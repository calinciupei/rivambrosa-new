import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Icon} from 'antd';

class ButtonComponent extends Component {
  constructor() {
    super();
    require('./styles/buttons.scss');

    this.handleClick = this.handleClick.bind(this);
  }

  state = {
    text: '',
    type: '',
    icon: '',
    iconRight: false,
  };

  static getDerivedStateFromProps(props) {
    const {text, type, icon, iconRight} = props;

    return {text, type, icon, iconRight};
  }

  renderIcon() {
    const {icon} = this.state;

    return <Icon type={icon} />;
  }

  handleClick(event) {
    const {calin} = this.props;

    calin(event);
  }

  render() {
    const {text, type, icon, iconRight} = this.state;

    return (
      <Button onClick={this.handleClick} className={type}>
        {!iconRight && icon && this.renderIcon()}
        {text}
        {iconRight && icon && this.renderIcon()}
      </Button>
    );
  }
}

ButtonComponent.defaultProps = {
  text: 'Add Text',
  type: 'primary',
  icon: '',
  iconRight: false,
  // eslint-disable-next-line no-empty-function
  calin: () => {},
};

ButtonComponent.propTypes = {
  type: PropTypes.string,
  text: PropTypes.string,
  icon: PropTypes.string,
  iconRight: PropTypes.bool,
  calin: PropTypes.func,
};

export default ButtonComponent;

import React from 'react';
import {Carousel} from 'antd';
import Button from './../button/button.component';

import './styles/carousel.scss';

const CarouselComponent = () => (
  <Carousel autoplay>
    <div className="carousel">
      <div className="carousel__header">
        PROVIDING <span className="carousel__header__stands">WORLD</span> CLASS
        <br/>
        FREIGHT SERVICES
      </div>
      <div className="carousel__info">
        Since 1890, when the business was founded in Germen, Australia, by Jeorge Vital Bin and Jeorge Binny Van,
        Truckpress has grown into one of the world's leading logistics providers.
      </div>
      <div className="carousel__actions-controls">
        <Button type="primary" text="Read More" iconRight={true} icon="caret-right"/>
        <Button type="secondary" text="Get a Quote" iconRight={true} icon="caret-right" />
      </div>
    </div>
    <div>
      <div className="carousel">
        <div className="carousel__header">
        PROVIDING <span className="carousel__header__stands">WORLD</span> CLASS
          <br/>
        FREIGHT SERVICES
        </div>
        <div className="carousel__info">
        Since 1890, when the business was founded in Germen, Australia, by Jeorge Vital Bin and Jeorge Binny Van,
        Truckpress has grown into one of the world's leading logistics providers.
        </div>
        <div className="carousel__actions-controls">
          <Button type="primary" text="Read More" iconRight={true} icon="caret-right"/>
          <Button type="secondary" text="Get a Quote" iconRight={true} icon="caret-right" />
        </div>
      </div>
    </div>
    <div>
      <div className="carousel">
        <div className="carousel__header">
        PROVIDING <span className="carousel__header__stands">WORLD</span> CLASS
          <br/>
        FREIGHT SERVICES
        </div>
        <div className="carousel__info">
        Since 1890, when the business was founded in Germen, Australia, by Jeorge Vital Bin and Jeorge Binny Van,
        Truckpress has grown into one of the world's leading logistics providers.
        </div>
        <div className="carousel__actions-controls">
          <Button type="primary" text="Read More" iconRight={true} icon="caret-right"/>
          <Button type="secondary" text="Get a Quote" iconRight={true} icon="caret-right" />
        </div>
      </div>
    </div>
    <div>
      <div className="carousel">
        <div className="carousel__header">
        PROVIDING <span className="carousel__header__stands">WORLD</span> CLASS
          <br/>
        FREIGHT SERVICES
        </div>
        <div className="carousel__info">
        Since 1890, when the business was founded in Germen, Australia, by Jeorge Vital Bin and Jeorge Binny Van,
        Truckpress has grown into one of the world's leading logistics providers.
        </div>
        <div className="carousel__actions-controls">
          <Button type="primary" text="Read More" iconRight={true} icon="caret-right"/>
          <Button type="secondary" text="Get a Quote" iconRight={true} icon="caret-right" />
        </div>
      </div>
    </div>
  </Carousel>
);

export default CarouselComponent;

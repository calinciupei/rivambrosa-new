/* eslint-disable react/no-danger */
import React, {Component} from 'react';
import {Layout, Row, Col} from 'antd';
import {library} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCheckSquare, faMapMarkerAlt, faPhoneSquareAlt, faEnvelopeSquare} from '@fortawesome/free-solid-svg-icons';
import {faFacebookF, faTwitter, faSkype, faGooglePlusG} from '@fortawesome/free-brands-svg-icons';
import data from './footer.data';
import './styles/footer.scss';

library.add(
  faFacebookF,
  faTwitter,
  faSkype,
  faGooglePlusG,
  faCheckSquare,
  faMapMarkerAlt,
  faPhoneSquareAlt,
  faEnvelopeSquare,
);

class FooterComponent extends Component {
  renderSocials() {
    const {firstColumn: {socials}} = data;

    return (
      <ul className="footer_socials">
        {socials.map((item, index) => (
          <li key={index}>
            <a className="social-link" target="_blank" href={item.link} aria-label={item.icon}>
              <FontAwesomeIcon icon={['fab', item.icon]} />
            </a>
          </li>
        ))}
      </ul>
    );
  }

  renderAbout() {
    const {firstColumn} = data;

    return (
      <Col xs={24} sm={24} md={24} lg={8} xl={6} className="footer__first">
        <img alt="Rivambrosa Srl" src="/images/logo.png" />
        <p>
          {firstColumn.text}
        </p>
        {this.renderSocials()}
      </Col>
    );
  }

  renderHelp({title, list}) {
    return (
      <Col xs={12} sm={12} md={8} lg={5} xl={3} className="main-footer__help">
        <div className="title">
          {title}
        </div>
        {this.renderList(list)}
      </Col>
    );
  }

  renderList(list) {
    return (
      <ul className="help-list">
        {list.map((item, index) => (
          <li key={index}>
            <span className="icon">
              <FontAwesomeIcon icon={item.icon} />
            </span>
            <span dangerouslySetInnerHTML={{__html: item.text}} />
          </li>
        ))}
      </ul>
    );
  }

  renderContactDetails({title, list}) {
    return (
      <Col xs={24} sm={24} md={8} lg={6} xl={6} className="main-footer__help">
        <div className="title">
          {title}
        </div>
        {this.renderList(list)}
      </Col>
    );
  }

  render() {
    const {secondColumn, thirdColumn, fourthColumn} = data;
    return (
      <Layout.Footer className="main-footer">
        <Row type="flex" justify="center" className="footer">
          {this.renderAbout()}
          {this.renderHelp(secondColumn)}
          {this.renderHelp(thirdColumn)}
          {this.renderContactDetails(fourthColumn)}
        </Row>
      </Layout.Footer>
    );
  }
}

export default FooterComponent;

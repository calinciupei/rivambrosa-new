/* eslint-disable quotes */
/* eslint-disable max-len */
export default {
  firstColumn: {
    text: "Since 1890, when the busine ss was founded in Germen, Australia, by Jeorge Vital Bin and Jeorge Binny Van, Truckpress has grown into one of the world's leading logistics providers. Road freight parcel delivery is the transfer andthe goods via road carrier.",
    socials: [
      {icon: 'facebook-f', link: ''},
      {icon: 'twitter', link: ''},
      {icon: 'skype', link: ''},
      {icon: 'google-plus-g', link: ''},
    ],
  },
  secondColumn: {
    title: 'Services',
    list: [
      {icon: 'check-square', text: 'All Services'},
      {icon: 'check-square', text: 'Air Freight'},
      {icon: 'check-square', text: 'Ocean Freight'},
      {icon: 'check-square', text: 'Raod and Rail Freight'},
      {icon: 'check-square', text: 'Warehousing'},
    ],
  },
  thirdColumn: {
    title: 'Useful links',
    list: [
      {icon: 'check-square', text: 'Tracking'},
      {icon: 'check-square', text: 'FAQ\'s'},
      {icon: 'check-square', text: 'Company News'},
      {icon: 'check-square', text: 'Who We Are?'},
      {icon: 'check-square', text: 'Our History'},
      {icon: 'check-square', text: 'Customer Care'},
    ],
  },
  fourthColumn: {
    title: 'Contact Details',
    list: [
      {icon: 'map-marker-alt', text: 'TP Limted ( HEADQUARTERS ) <br> <small>0498 Brooklyn Street, 7th Cross  United Kindom</small>'},
      {icon: 'phone-square-alt', text: 'Phone - 0422-3457890, 0422-4569870'},
      {icon: 'envelope-square', text: 'Write us - Truckpress@Steelthemes.com'},
    ],
  },
};

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {getPlatformConfig, getDependencySuccess} from '../../data/actions/platform-config';

class InitializeState extends Component {
  state = {
    dependencyRequestStatus: {
      platformConfig: this.props.initializeState.platformConfig,
    },
    error: false,
    ready: false,
  };

  static getDerivedStateFromProps(props) {
    const {initializeState: {
      done,
      error,
      platformConfig,
      success,
    }} = props;

    return {
      dependencyRequestStatus: {
        platformConfig: platformConfig,
      },
      error: error,
      ready: done && success,
    };
  }

  componentDidMount() {
    this.props.dispatch(getPlatformConfig());
  }

  componentDidUpdate() {
    const {platformConfig} = this.state.dependencyRequestStatus;
    const {done, error} = this.props.initializeState;

    if (error) {
      return;
    }

    if (!done) {
      if (platformConfig) {
        this.props.dispatch(getDependencySuccess());
      }
    }
  }

  render() {
    const {children} = this.props;
    const {error, ready} = this.state;
    if (error) {
      return (
        <div>Something went wrong!</div>
      );
    }

    if (ready) {
      return (
        <div className="main">
          <div className="main__container">{children}</div>
        </div>
      );
    }

    return false;
  }
}

InitializeState.propTypes = {
  children: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  initializeState: PropTypes.object.isRequired,
};

export default InitializeState;

import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';
import InitializeState from './initialize-state.component';

export const mapStateToProps = state => {
  const {initializeState} = state;

  return ({
    initializeState,
  });
};

export default connect(mapStateToProps)(withTranslation()(InitializeState));

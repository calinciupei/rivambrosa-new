
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Layout} from 'antd';
import NavigationMenu from './../../navigation-menu/navigation-menu.container';
import TopHeader from './../../top-header/top-header.component';
import Logo from './../../logo/logo.component';
import Button from './../../button/button.component';
import FooterComponent from './../../../components/footer/footer.component';

import './styles/home.scss';

const {Content, Header} = Layout;

class HomeLayout extends Component {
  renderMainHeader() {
    return (
      <div className="main-header">
        <Logo />
        <div className="cta-group">
          <NavigationMenu />
          <Button type="secondary" text="Get a Quote" iconRight={true} icon="caret-right"/>
        </div>
      </div>
    );
  }

  renderFooter() {
    return <FooterComponent />;
  }

  render() {
    const {children} = this.props;

    return (
      <Layout className="layout">
        <TopHeader />
        <Header>
          {this.renderMainHeader()}
        </Header>
        <Content>
          {children}
        </Content>
        {this.renderFooter()}
      </Layout>
    );
  }
}

HomeLayout.propTypes = {
  children: PropTypes.object.isRequired,
};

export default HomeLayout;

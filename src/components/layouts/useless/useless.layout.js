import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import LocaleMenu from './../../locale-menu/locale-menu.container';

class UselessLayout extends Component {
  render() {
    const {children} = this.props;

    return (
      <Fragment>
        <Box display="flex" flexDirection="row-reverse">
          <LocaleMenu />
        </Box>
        {children}
      </Fragment>
    );
  }
}

UselessLayout.propTypes = {
  children: PropTypes.object.isRequired,
};

export default UselessLayout;

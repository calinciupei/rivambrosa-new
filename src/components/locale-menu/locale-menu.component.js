import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';
import Icon from '@material-ui/core/Icon';

class LocaleMenu extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  state = {
    open: false,
    locale: 'ro',
    anchorEl: null,
  };

  handleClick(event) {
    this.setState({
      anchorEl: event.currentTarget,
      open: true,
    });
  }

  handleClose(event) {
    const {i18n} = this.props;
    const {currentTarget} = event;
    const locale = currentTarget.getAttribute('locale');

    this.setState({open: false, locale});

    i18n.changeLanguage(locale);
  }

  selectedLocale() {
    const {locale} = this.state;

    return (
      <Icon>
        <img alt={`language-${locale}`} src={`/images/${locale}.svg`}/>
      </Icon>
    );
  }

  render() {
    const {open, anchorEl} = this.state;
    const {classes} = this.props;

    return (
      <div>
        <Button
          className={classes.selectedLocaleButton}
          aria-controls="fade-menu"
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          {this.selectedLocale()}
        </Button>
        <Menu
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={this.handleClose}
          TransitionComponent={Fade}
        >
          <MenuItem locale="en" onClick={this.handleClose}>
            <Icon>
              <img alt="en" src="/images/en.svg"/>
            </Icon>
          </MenuItem>
          <MenuItem locale="ro" onClick={this.handleClose}>
            <Icon>
              <img alt="ro" src="/images/ro.svg"/>
            </Icon>
          </MenuItem>
        </Menu>
      </div>
    );
  }
}

LocaleMenu.propTypes = {
  classes: PropTypes.object,
  i18n: PropTypes.object,
};

export default LocaleMenu;

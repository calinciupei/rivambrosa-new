import {withStyles} from '@material-ui/styles';
import {withTranslation} from 'react-i18next';
import LocaleMenu from './locale-menu.component';

const styles = {
  selectedLocaleButton: {
    backgroundColor: 'transparent',
    padding: 0,
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
};

export default withStyles(styles)(withTranslation()(LocaleMenu));

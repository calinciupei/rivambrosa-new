import React from 'react';

import './styles/logo.scss';

const Logo = () => (
  <div className="logo">
    <img alt="Rivambrosa Srl" src="/images/logo.png" />
  </div>
);

export default Logo;

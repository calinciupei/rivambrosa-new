import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {getNavigationMenu} from './../../data/actions/navigation-menu';
import {Menu} from 'antd';
import {Link} from 'react-router-dom';

class NavigationMenu extends Component {
  constructor() {
    super();
    require('./styles/navigation-menu.scss');
  }

  state = {
    data: null,
    error: false,
    inPorgress: false,
    success: false,
    pathName: '',
    currentMenu: 'home',
  };

  static getDerivedStateFromProps(props) {
    const {
      navigationState: {data, error, inPorgress, success},
      location: {pathname},
    } = props;

    return {data, error, inPorgress, pathName: pathname, success};
  }

  componentDidMount() {
    this.props.dispatch(getNavigationMenu());
  }

  renderItemNavigation(item) {
    const {localeKeyLabel, linkUrl} = item;

    if (localeKeyLabel) {
      return (
        <Menu.Item key={localeKeyLabel}>
          <Link to={linkUrl}>
            {localeKeyLabel}
          </Link>
        </Menu.Item>
      );
    }
  }

  handleMenuState = e => {
    this.setState({currentMenu: e.key});
  }

  handleMenuCurrentKey() {
    const {pathName, currentMenu} = this.state;
    const pathReplaced = pathName.replace('/', '');

    return pathReplaced ? pathReplaced : currentMenu;
  }

  render() {
    const {data, error, success} = this.state;
    const currentMenu = this.handleMenuCurrentKey();

    if (error) {
      return (
        <div>Something went wrong!</div>
      );
    }

    if (data && success) {
      return (
        <Menu
          theme="light"
          mode="horizontal"
          defaultSelectedKeys={['2']}
          style={{lineHeight: '64px'}}
          selectedKeys={[currentMenu]}
          onClick={this.handleMenuState}
        >
          {
            data.map((item) => this.renderItemNavigation(item))
          }
        </Menu>
      );
    }

    return <div/>;
  }
}

NavigationMenu.propTypes = {
  dispatch: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  navigationState: PropTypes.object.isRequired,
};

export default NavigationMenu;

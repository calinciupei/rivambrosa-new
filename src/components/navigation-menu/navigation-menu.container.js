import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';
import {withRouter} from 'react-router-dom';
import NavigationMenu from './navigation-menu.component';

const mapStateToProps = state => {
  const {navigationState} = state;

  return ({navigationState});
};

export default connect(mapStateToProps)(withTranslation()(withRouter(NavigationMenu)));

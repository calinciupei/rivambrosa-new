import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Form,
  Input,
  Select,
} from 'antd';
import ButtonComponent from './../button/button.component';

import './styles/quick-quote-form.scss';

const {Option} = Select;
const {TextArea} = Input;

class QuickQuoteForm extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        // eslint-disable-next-line no-console
        console.log('Received values of form: ', values);
      }
    });
  };

  handleConfirmBlur = e => {
    const {value} = e.target;
    this.setState({confirmDirty: this.state.confirmDirty || !!value});
  };

  compareToFirstPassword = (rule, value, callback) => {
    const {form} = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const {form} = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], {force: true});
    }
    callback();
  };

  handleWebsiteChange = value => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
    }
    this.setState({autoCompleteResult});
  };

  render() {
    const {getFieldDecorator} = this.props.form;

    const formItemLayout = {
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 24},
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
        },
        sm: {
          span: 24,
        },
      },
    };

    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item>
          {getFieldDecorator('nickname', {
            rules: [{required: true, message: 'Please input your name!', whitespace: true}],
          })(<Input placeholder="Name*" />)}
        </Form.Item>

        <Form.Item>
          {getFieldDecorator('email', {
            rules: [
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ],
          })(<Input placeholder="Email*" />)}
        </Form.Item>

        <Form.Item className="inline-controls">
          <Form.Item style={{display: 'inline-block', width: 'calc(50% - 10px'}}>
            {getFieldDecorator('subject', {})(
              <Input
                placeholder="Subject"
              />,
            )}
          </Form.Item>

          <Form.Item style={{display: 'inline-block', width: 'calc(50% - 10px'}}>
            {getFieldDecorator('activity', {})(
              <Select
                showSearch
                placeholder="Select activity"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                <Option value="air-freight">Air Frieght</Option>
                <Option value="sea-freight">Sea Frieght</Option>
                <Option value="road-freight">Road Frieght</Option>
                <Option value="ware-housing">Ware Housing</Option>
              </Select>,
            )}
          </Form.Item>
        </Form.Item>

        <Form.Item>
          {getFieldDecorator('message', {})(
            <TextArea placeholder="Message" rows={4} />,
          )}
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          <ButtonComponent calin={this.handleSubmit} type="primary" htmlType="submit">
            Register
          </ButtonComponent>
        </Form.Item>
      </Form>
    );
  }
}

QuickQuoteForm.propTypes = {
  form: PropTypes.object,
  getFieldValue: PropTypes.func,
};

const QuickQuoteFormComponent = Form.create({name: 'register'})(QuickQuoteForm);

export default QuickQuoteFormComponent;

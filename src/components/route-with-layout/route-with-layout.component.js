import React, {Suspense} from 'react';
import PropTypes from 'prop-types';
import {Route} from 'react-router-dom';

import UselessLayout from '../layouts/useless/useless.layout';
import HomeLayout from '../layouts/home/home.layout';
import SuspenseLoading from './../../components/suspense-loading/suspense-loading.component';

const Layouts = {
  UselessLayout,
  HomeLayout,
};

const RouteWithLayout = ({component: Component, layout, ...rest}) => {
  const Layout = Layouts[layout];

  return (
    <Route
      {...rest}
      render={props => (
        <Layout>
          <Suspense fallback={<SuspenseLoading />}>
            <Component {...props} />
          </Suspense>
        </Layout>
      )}
    />
  );
};

RouteWithLayout.propTypes = {
  component: PropTypes.func.isRequired,
  layout: PropTypes.string.isRequired,
};

export default RouteWithLayout;

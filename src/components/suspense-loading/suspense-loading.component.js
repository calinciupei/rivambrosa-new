import React from 'react';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';

const SuspenseLoading = () => (
  <Grid
    container
    spacing={0}
    alignItems="center"
    justify="center"
    style={{
      minHeight: '100vh',
      textAlign: 'center',
    }}
  >
    <CircularProgress />
  </Grid>
);

export default SuspenseLoading;

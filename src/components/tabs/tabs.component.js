/* eslint-disable no-empty-function */
/* eslint-disable react/no-multi-comp */
import React, {createContext, useState, useContext} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Animate from 'rc-animate';

import './styles/tabs.scss';

const TabContext = createContext();

function Tabs(props) {
  const {initialValue, className = '', children, ...restProps} = props;

  const [activeTab, changeTab] = useState(initialValue);
  const tabProviderValue = {activeTab, changeTab};
  const classes = classNames('tabs', {
    className,
  });

  return (
    <TabContext.Provider value={tabProviderValue}>
      <div className={classes} {...restProps}>{children}</div>
    </TabContext.Provider>
  );
}

function TabList(props) {
  const {className = '', children, ...restProps} = props;
  const classes = classNames('tab-list', {
    className,
  });

  return (
    <div className={classes} {...restProps}>
      {children}
    </div>
  );
}

function Tab(props) {
  const {name, className = '', onClick = () => {}, children, ...restProps} = props;
  const tabContext = useContext(TabContext);
  const classes = classNames(className, 'tab', {
    'active': tabContext.activeTab === name,
  });

  const handleClick = event => {
    tabContext.changeTab(name);
    onClick(event);
  };

  return (
    <span className={classes} onClick={handleClick} {...restProps}>
      {children}
    </span>
  );
}

function TabPanel(props) {
  const {name, className = '', children, ...restProps} = props;
  const tabContext = useContext(TabContext);
  const classes = classNames('tab-panel', {
    className,
  });

  return (
    tabContext.activeTab === name && (
      <Animate
        transitionName="fade"
        transitionAppear
        className={classes}
        {...restProps}
      >
        {children}
      </Animate>
    )
  );
}

function TabDivider() {
  return <div className="tab-driver" />;
}

Tabs.propTypes = {
  initialValue: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.array,
};

TabList.propTypes = {
  className: PropTypes.string,
  children: PropTypes.array,
};

Tab.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.array,
  onClick: PropTypes.func,
};

Tabs.List = TabList;
Tabs.Tab = Tab;
Tabs.Panel = TabPanel;
Tabs.Divider = TabDivider;

export {Tabs, TabList, Tab, TabPanel, TabDivider};

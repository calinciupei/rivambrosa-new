import React, {Component} from 'react';
import classnames from 'classnames';
import {Icon} from 'antd';
import {library} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFacebookF, faInstagram, faTwitterSquare} from '@fortawesome/free-brands-svg-icons';
import {faEnvelopeSquare} from '@fortawesome/free-solid-svg-icons';

library.add(faFacebookF, faInstagram, faTwitterSquare, faEnvelopeSquare);

class TopHeader extends Component {
  constructor() {
    super();
    require('./styles/top-header.scss');
  }

  state = {
    topList: [
      {text: 'Mon -Sat: 08.00 - 20.00', iconClass: 'clock-circle', hasText: true, link: ''},
      {text: '+40 747 814 680', iconClass: 'phone', hasText: true, link: ''},
      {text: 'office@rivambrosa.com', iconClass: 'envelope-square', iconType: 'fas', hasText: true, link: 'mailto:office@rivambrosa.com'},
      {text: '', iconClass: 'facebook-f', iconType: 'fab', link: 'https://www.facebook.com/rivambrosasrl'},
      {text: '', iconClass: 'instagram', iconType: 'fab', link: 'https://www.facebook.com/rivambrosasrl'},
      {text: '', iconClass: 'twitter-square', iconType: 'fab', link: 'https://www.facebook.com/rivambrosasrl'},
    ],
  }

  renderList() {
    const {topList} = this.state;

    return (
      <ul className="top-header__list">
        {topList.map((item, index) => this.renderItemList(item, index))}
      </ul>
    );
  }

  renderItemList(item, key) {
    let itemList;
    const {link, text, hasText, iconClass, iconType} = item;
    const classes = classnames({
      'top-header__list__item': true,
      'top-header__list__item--text': hasText,
      'top-header__list__item--icon': !hasText,
    });

    if (link && link !== undefined) {
      itemList = () => (
        <a href={link} aria-label={iconClass}>
          <>
            {iconType === 'fab' && <FontAwesomeIcon icon={['fab', iconClass]} />}
            {iconType === 'fas' && <FontAwesomeIcon icon={iconClass} />}
            {text}
          </>
        </a>
      );
    } else {
      itemList = () => (
        <>
          <Icon type={iconClass} />
          {text}
        </>
      );
    }

    return (
      <li key={key} className={classes}>
        {itemList()}
      </li>
    );
  }

  render() {
    return (
      <div className="top-header">
        {this.renderList()}
      </div>
    );
  }
}

export default TopHeader;

import {NAV_REQUEST, NAV_RECEIVE_FAILURE, NAV_RECEIVE_SUCCESS} from './../constants/nav.constant';
import service from '../services/navigation-menu';
const privateService = {};

privateService.getNavigationMenu = () => dispatch => {
  dispatch({type: NAV_REQUEST});

  service.getNavigation()
    .then(response => {
      dispatch({payload: response, type: NAV_RECEIVE_SUCCESS});
    })
    .catch(error => {
      dispatch({payload: error, type: NAV_RECEIVE_FAILURE});
    });
};

privateService.retrieveNavigationMenu = (state) => {
  const navState = state.navigationState;

  if (!navState.actionInProgress && !navState.data) {
    return true;
  }

  return false;
};

const getNavigationMenu = () => (dispatch, getState) => {
  if (privateService.retrieveNavigationMenu(getState())) {
    dispatch(privateService.getNavigationMenu());
  }
};

export {getNavigationMenu};

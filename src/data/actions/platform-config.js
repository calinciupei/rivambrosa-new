import {
  DEPENDENCY_DONE,
  INIT_APP_REQUEST,
  INIT_APP_RECEIVE_SUCCESS,
  INIT_APP_RECEIVE_FAILURE,
} from '../constants/init.constants';
import service from '../services/platform-config';

const privateService = {};

privateService.shouldGet = (state) => {
  const initAppStore = state.initializeState;

  if (!initAppStore.actionInProgress && !initAppStore.data) {
    return true;
  }

  return false;
};

privateService.retrievePlatformConfig = () => dispatch => {
  dispatch({
    type: INIT_APP_REQUEST,
  });

  if (window.platfomConfig && Object.keys(window.platfomConfig).length > 0) {
    dispatch({
      payload: window.platfomConfig,
      type: INIT_APP_RECEIVE_SUCCESS,
    });

    return;
  }

  service.getPlatformConfig()
    .then(
      response => {
        dispatch({payload: response, type: INIT_APP_RECEIVE_SUCCESS});
      },
      error => {
        dispatch({payload: error, type: INIT_APP_RECEIVE_FAILURE});
      },
    );
};

const getPlatformConfig = () => (dispatch, getState) => {
  if (privateService.shouldGet(getState())) {
    dispatch(privateService.retrievePlatformConfig());
  }
};

const getDependencySuccess = () => (dispatch) => (
  dispatch({
    type: DEPENDENCY_DONE,
  })
);

export {getPlatformConfig, getDependencySuccess};

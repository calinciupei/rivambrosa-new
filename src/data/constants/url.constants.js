const NAVIGATION_URL = '/mocks/navigation.json';

const PLATFORM_CONFIG = '/mocks/platform-config.json';

export {
  NAVIGATION_URL,
  PLATFORM_CONFIG,
};

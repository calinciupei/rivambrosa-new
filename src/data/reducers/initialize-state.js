import {
  DEPENDENCY_DONE,
  INIT_APP_REQUEST,
  INIT_APP_RECEIVE_SUCCESS,
  INIT_APP_RECEIVE_FAILURE,
} from '../constants/init.constants';

const initialState = {
  actionInProgress: false,
  platformConfig: false,
  done: false,
  success: null,
  data: null,
};

const initializeState = (state = initialState, action) => {
  const {payload, type} = action;

  switch (type) {
    case INIT_APP_REQUEST:
      return Object.assign({}, state, {
        actionInProgress: true,
        platformConfig: false,
      });

    case INIT_APP_RECEIVE_SUCCESS:
      return Object.assign({}, state, {
        actionInProgress: false,
        data: payload,
        platformConfig: true,
      });

    case INIT_APP_RECEIVE_FAILURE:
      return Object.assign({}, state, {
        actionInProgress: false,
        data: payload,
        error: true,
        platformConfig: false,
        success: false,
      });

    case DEPENDENCY_DONE:
      return Object.assign({}, state, {
        success: true,
        done: true,
      });

    default:
      return state;
  }
};

export default initializeState;

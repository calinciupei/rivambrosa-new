import {NAV_REQUEST, NAV_RECEIVE_FAILURE, NAV_RECEIVE_SUCCESS} from '../constants/nav.constant';

const initialState = {
  inProgress: false,
  error: false,
  success: false,
  data: null,
};

const navigationState = (state = initialState, action) => {
  const {payload, type} = action;

  switch (type) {
    case NAV_REQUEST:
      return Object.assign({}, state, {
        inProgress: true,
      });

    case NAV_RECEIVE_SUCCESS:
      return Object.assign({}, state, {
        inProgress: false,
        success: true,
        data: payload,
      });

    case NAV_RECEIVE_FAILURE:
      return Object.assign({}, state, {
        inProgress: false,
        error: true,
        success: false,
        data: payload,
      });

    default:
      return state;
  }
};

export default navigationState;

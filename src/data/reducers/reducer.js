import {combineReducers} from 'redux';
import initializeState from './initialize-state';
import navigationState from './navigation-menu';

export const getReducers = () =>
  combineReducers({
    initializeState,
    navigationState,
  });

import axios from 'axios';

// make request using axios
const makeRequest = (method, url, headers = {}, params = {}) => {
  const additionalHeaders = {...headers};
  let requestParams = {};
  let requestData = {};

  if (method === 'get') {
    requestParams = {...params};
  } else {
    requestData = {...params};
  }

  return new Promise((resolve, reject) => {
    axios.request({
      method,
      url,
      headers: additionalHeaders,
      data: requestData,
      params: requestParams,
      withCredentials: true, // allow to make cors requests
      crossDomain: true,
    })
      .then((response) => {
        const {
          data,
          status,
        } = response;

        if (status >= 200 && status <= 300) {
          resolve(data);
        } else {
          reject(data);
        }
      })
      .catch((error) => {
        const {response} = error;

        // if an ajax call is made but the user is not logged in or doesn't have a limited session, the server will
        // response with a 401 error code on which we will redirect him to the identity sso page.
        if (response && response.status === 401) {
          reject(error);
        }

        reject(error);
      });
  });
};

// get request
const _get = (path, options = {}) => {
  const {
    headers,
  } = options;
  const params = {
    ...options.params,
    TS: `_${Date.now()}`,
  };

  return makeRequest('get', path, headers, params);
};

// post request
const _post = (path, options = {}) => {
  const {
    headers,
  } = options;
  const params = {
    ...options.params,
  };

  return makeRequest('post', path, headers, params);
};

// service interface
const service = {
  get: _get,
  post: _post,
};

/**
 * @example
 * const path = 'http://www.google.com/api/getUser';
 * const options = {
 *   headers: {
 *     myCustomHeader: 'My value',
 *   },
 *   params: {
 *     propertyA: 1,
 *     propertyB: 2,
 *   },
 * };
 *
 * apiService.get(path, options).then().catch()
 * apiService.post(path, options).then().catch()
 */

// service that handles all the API requests
export default service;

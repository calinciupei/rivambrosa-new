import apiService from '../services/api.service';
import {NAVIGATION_URL} from '../constants/url.constants';

const getNavigation = () => {
  const path = NAVIGATION_URL;

  return new Promise((resolve, reject) => {
    apiService.get(path)
      .then((response) => {
        resolve(response);
      })
      .catch((reason) => {
        reject(reason);
      });
  });
};

export default {getNavigation};

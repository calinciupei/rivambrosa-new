import apiService from './../../data/services/api.service';
import {PLATFORM_CONFIG} from './../constants/url.constants';

const getPlatformConfig = () => {
  const path = PLATFORM_CONFIG;

  return new Promise((resolve, reject) => {
    apiService.get(path)
      .then((response) => {
        resolve(response);
      })
      .catch((reason) => {
        reject(reason);
      });
  });
};

const getWindowPlatformConfig = () => window.maxPlatformConfig || {};

const getUserContext = () => window.maxPlatformConfig.userContext || {};

const service = {
  getPlatformConfig,
  getWindowPlatformConfig,
  getUserContext,
};

export default service;

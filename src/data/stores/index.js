import {applyMiddleware, compose, createStore} from 'redux';
import {logger, thunk} from '../../middleware';
import {getReducers} from '../reducers/reducer';

const middleware = [thunk];
const enhancers = [];

if (ENVIRONMENT === 'development') {
  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__());
  }
  middleware.push(logger);
}

const store = createStore(
  getReducers(),
  compose(
    applyMiddleware(...middleware),
    ...enhancers,
  ),
);

store.asyncReducers = {};

export default store;

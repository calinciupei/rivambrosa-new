import React, {Suspense} from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter, Switch} from 'react-router-dom';
import AsyncComponent from './routes/routes';
import InitializeState from './components/initialize-state/initialize-state.container';
import RouteWithLayout from './components/route-with-layout/route-with-layout.component';
import SuspenseLoading from './components/suspense-loading/suspense-loading.component';
import ErrorBoundary from './components/error-boundary/error-boundary.component';
import store from './data/stores';
import './i18n';
// styles
import './style/main.scss';

const rootNode = document.getElementById('dom-r-root');

render(
  <ErrorBoundary>
    <Suspense fallback={<SuspenseLoading />}>
      <Provider store={store}>
        <InitializeState>
          <BrowserRouter>
            <Switch>
              <RouteWithLayout
                exact
                path="/"
                layout="HomeLayout"
                component={() => <AsyncComponent componentName="HomePage"/>}
              />
              <RouteWithLayout
                path="/home"
                layout="HomeLayout"
                component={() => <AsyncComponent componentName="HomePage"/>}
              />
              <RouteWithLayout
                path="/about-us"
                layout="HomeLayout"
                component={() => <AsyncComponent componentName="AboutUs"/>}
              />
              <RouteWithLayout
                layout="HomeLayout"
                component={() => ('404 No Found')}
              />
            </Switch>
          </BrowserRouter>
        </InitializeState>
      </Provider>
    </Suspense>
  </ErrorBoundary>,
  rootNode,
);

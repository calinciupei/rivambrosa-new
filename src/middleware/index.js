import thunk from 'redux-thunk';
import logger from './logger';

export {logger, thunk};

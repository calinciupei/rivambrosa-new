import {createLogger} from 'redux-logger';

const logger = createLogger({
  collapsed: true,
  duration: true,
  timestamp: true,
  logErrors: true,
  level: 'log',
  diff: true,
});

export default logger;

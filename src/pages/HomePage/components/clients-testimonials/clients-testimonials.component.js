import React, {Component} from 'react';
import {Col, Row} from 'antd';
import data from './clients-testimonials.data';

class ClientsTestimonials extends Component {
  constructor() {
    super();
    require('./styles/clients-testimonials.scss');
  }

  state = {
    data,
  }

  render() {
    const {data} = this.state;

    return (
      <div className="clients-testimonials">
        <div className="clients-testimonials__header">
          <span>Valuable Words From Clients</span>
        </div>
        <Row type="flex" justify="center" className="clients-testimonials__items">
          {data.map((item, index) => (
            <Col key={index} xs={14} sm={14} md={8} lg={8} xl={6} className="clients-testimonials__item">
              <div className="clients-testimonials__icon"/>
              <div className="clients-testimonials__description">
                {item.description}
              </div>
              <div className="clients-testimonials__logo">
                <img alt="Rivambrosa Srl" src={`/images/${item.logo}`} />
              </div>
            </Col>
          ))}
        </Row>
      </div>
    );
  }
}

export default ClientsTestimonials;

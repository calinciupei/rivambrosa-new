import React, {Component} from 'react';
import {Col, Row} from 'antd';
import {library} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faUniversity, faThumbsUp, faCubes} from '@fortawesome/free-solid-svg-icons';
import {Tabs, TabList, Tab, TabPanel} from './../../../../components/tabs/tabs.component';
import Button from '../../../../components/button/button.component';
import data from './company-values.data';

import './styles/company-values.scss';

library.add(faUniversity, faThumbsUp, faCubes);

class CompanyValuesComponent extends Component {
  renderTabContent(tab) {
    const {title, desc, list} = tab;

    return (
      <div className="tab-content">
        <div className="tab-content__title">
          {title}
        </div>
        <p>
          {desc}
        </p>
        <div className="tab-content__list">
          {this.renderTabContentList(list)}
        </div>
        <div className="tab-content__action">
          <Button type="primary" text="Read More" iconRight={true} icon="caret-right"/>
        </div>
      </div>
    );
  }

  renderTabContentList(list) {
    return (
      <ul>
        {list.map((item, index) => <li key={index}>{item}</li>)}
      </ul>
    );
  }

  render() {
    return (
      <Row type="flex" justify="center" className="company-values">
        <Col xs={24} sm={24} md={24} lg={24} xl={18} className="company-values__item">
          <Tabs initialValue={data[0].id}>
            <TabList>
              {data.map((item, index) => (
                <Tab key={index} name={item.id} className={item.id}>
                  <FontAwesomeIcon icon={item.icon} />
                  {item.name}
                </Tab>
              ))}
            </TabList>
            {data.map((item, index) => (
              <TabPanel key={index} name={item.id}>
                {this.renderTabContent(item.tab)}
              </TabPanel>
            ))}
          </Tabs>
        </Col>
      </Row>
    );
  }
}

export default CompanyValuesComponent;

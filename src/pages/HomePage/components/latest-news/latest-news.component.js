import React, {Component} from 'react';
import {Col, Row, Button} from 'antd';
import {library} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFacebookF, faTwitter, faGooglePlusG, faLinkedinIn} from '@fortawesome/free-brands-svg-icons';
import {faCaretRight, faCaretLeft} from '@fortawesome/free-solid-svg-icons';
import './styles/latest-news.scss';

library.add(faFacebookF, faTwitter, faGooglePlusG, faLinkedinIn);

class LatestNewsComponent extends Component {
  render() {
    return (
      <div className="latest-news">

        <Row type="flex" justify="center">
          <Col xs={24} sm={24} md={18} lg={18} xl={18}>
            <div className="latest-news__header">
              <div className="latest-news__title">Latest News</div>
              <div className="latest-news__arrows">
                <Button disabled>
                  <FontAwesomeIcon icon={faCaretLeft} />
                </Button>
                <Button>
                  <FontAwesomeIcon icon={faCaretRight} />
                </Button>
              </div>
            </div>
          </Col>
        </Row>

        <Row type="flex" justify="center">
          <Col xs={24} sm={24} md={18} lg={18} xl={18} className="latest-news__carousel">
            <div className="latest-news__item">
              <div className="latest-news__tag">
                Services, Trucking
              </div>
              <div className="latest-news__news-title">
                Colombia Summit - Intelligent Green Logistic Solution
              </div>
              <div className="latest-news__content">
                Trukpress Loves or pursues or desires to obtain pain of itself, because it is pain,
                but because of occasionally circumstances occur in which toil and pain can procure
                him some great pleasure. To take a trivial example, which of us ever undertakes
                logistic laborious physically exercise, except to obtain some advantage from it?
                But who will has any right to findout fault with a man who chooses to enjoy a pleasure
                that has no annoying labore visit consequences or onepain that produces.
              </div>
              <div className="latest-news__actions">
                <div className="latest-news__socials">
                  <div className="latest-news__socials__text">
                    Share:
                  </div>
                  <div className="latest-news__socials__links">
                    <ul>
                      <li><a href="" alt="facebook"><FontAwesomeIcon icon={['fab', 'facebook-f']}/></a></li>
                      <li><a href="" alt="twitter"><FontAwesomeIcon icon={['fab', 'twitter']}/></a></li>
                      <li><a href="" alt="google"><FontAwesomeIcon icon={['fab', 'google-plus-g']}/></a></li>
                      <li><a href="" alt="linkedin"><FontAwesomeIcon icon={['fab', 'linkedin-in']}/></a></li>
                    </ul>
                  </div>
                </div>
                <div className="latest-news__read-more">
                  <a href="" alt="Read more" target="_self">
                    <span>Read More</span>
                    <FontAwesomeIcon icon={faCaretRight} />
                  </a>
                </div>
              </div>
            </div>

            <div className="latest-news__item">
              <div className="latest-news__tag">
                Services, Trucking
              </div>
              <div className="latest-news__news-title">
                Colombia Summit - Intelligent Green Logistic Solution
              </div>
              <div className="latest-news__content">
                Trukpress Loves or pursues or desires to obtain pain of itself, because it is pain,
                but because of occasionally circumstances occur in which toil and pain can procure
                him some great pleasure. To take a trivial example, which of us ever undertakes
                logistic laborious physically exercise, except to obtain some advantage from it?
                But who will has any right to findout fault with a man who chooses to enjoy a pleasure
                that has no annoying labore visit consequences or onepain that produces.
              </div>
              <div className="latest-news__actions">
                <div className="latest-news__socials">
                  <div className="latest-news__socials__text">
                    Share:
                  </div>
                  <div className="latest-news__socials__links">
                    <ul>
                      <li><a href="" alt="facebook"><FontAwesomeIcon icon={['fab', 'facebook-f']}/></a></li>
                      <li><a href="" alt="twitter"><FontAwesomeIcon icon={['fab', 'twitter']}/></a></li>
                      <li><a href="" alt="google"><FontAwesomeIcon icon={['fab', 'google-plus-g']}/></a></li>
                      <li><a href="" alt="linkedin"><FontAwesomeIcon icon={['fab', 'linkedin-in']}/></a></li>
                    </ul>
                  </div>
                </div>
                <div className="latest-news__read-more">
                  <a href="" alt="Read more" target="_self">
                    <span>Read More</span>
                    <FontAwesomeIcon icon={faCaretRight} />
                  </a>
                </div>
              </div>
            </div>
          </Col>
        </Row>

      </div>
    );
  }
}
export default LatestNewsComponent;

import React, {Component} from 'react';
import {Col, Row, Icon} from 'antd';

import './styles/sales-department-info.scss';

class SalesDepartmentInfoComponent extends Component {
  render() {
    return (
      <Row type="flex" justify="center" className="sales-department-info">
        <Col xs={24} sm={24} md={24} lg={7} xl={7} className="sales-department-info sales-department-info--call">
          <div className="info-call-wrapper">
            <div className="icon-wrapper">
              <Icon type="phone" theme="filled" />
            </div>
            <div className="text-wrapper">
              For Any Help <br/>or queries
            </div>
          </div>
        </Col>
        <Col xs={24} sm={24} md={24} lg={17} xl={17} className="sales-department-info sales-department-info--contact">
          <span className="info-contact--desc">Call Us Now On</span>
          <span className="info-contact--phone">0422-3457890</span>
        </Col>
      </Row>
    );
  }
}

export default SalesDepartmentInfoComponent;

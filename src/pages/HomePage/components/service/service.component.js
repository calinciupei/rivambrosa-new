import React from 'react';
import {Layout, Row, Col} from 'antd';
import data from './service.data';

const {Content} = Layout;

const Service = () => (
  <Content className="home-page__services">
    <Row type="flex" justify="center">
      {data.map((item, index) => (
        <Col key={index} xs={10} sm={10} md={5} lg={4} xl={4} className="service-item">
          <div className="service-item__icon">
            <img width="64px" height="auto" alt={item.title} src={`/images/${item.image}`} />
          </div>
          <div className="service-item__title">
            {item.title}
          </div>
          <div className="service-item__text">
            {item.text}
          </div>
        </Col>
      ))}
    </Row>
  </Content>
);

export default Service;

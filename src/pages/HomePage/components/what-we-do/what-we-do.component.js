import React, {Component} from 'react';
import {Col, Row} from 'antd';
import data from './what-we-do.data';
import QuickQuoteFromComponent from './../../../../components/quick-quote-form/quick-quote-form.component';

import './styles/what-we-do.scss';

class WhatWeDoComponent extends Component {
  render() {
    return (
      <div className="what-we-do">
        <Row type="flex" justify="center" className="what-we-do">
          <Col xs={24} sm={24} md={24} lg={12} xl={9} className="what-we-do__item">
            <div className="what-we-do__title">
              <span>What we do?</span>
            </div>
            <div className="what-we-do__desc">
              In 1978, we laid out our goals for the future in our Strategy 2015,
              including our vision and mission for our company.
              Our vision emphasizes that we want to be The Logistics Company for
              the world as a global company, we are  over 220 countries.
            </div>
            <div className="what-we-do__activities">
              <ul className="activities-list">
                {data.map((item, index) => (
                  <li key={index} className={`activities-item ${item.icon}`}>
                    <div><b>{item.title}</b></div>
                    <div>{item.smallDesc}</div>
                  </li>
                ))}
              </ul>
            </div>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={9} className="what-we-do__item">
            <div className="what-we-do__title">
              <span>Request a Quick Quote</span>
            </div>
            <div className="quick-quote-form">
              <QuickQuoteFromComponent />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default WhatWeDoComponent;

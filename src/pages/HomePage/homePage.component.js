import React, {Component} from 'react';

import CarouselComponent from './../../components/carousel/carousel.component';
import Service from './components/service/service.component';
import ClientsTestimonials from './components/clients-testimonials/clients-testimonials.component';
import WhatWeDoComponent from './components/what-we-do/what-we-do.component';
import SalesDepartmentInfoComponent from './components/sales-department-info/sales-department-info.component';
import CompanyValuesComponent from './components/company-values/company-values.component';
import LatestNewsComponent from './components/latest-news/latest-news.component';

import './styles/home-page.scss';

class HomePage extends Component {
  render() {
    return (
      <div className="home-page">
        <CarouselComponent />
        <Service />
        <ClientsTestimonials />
        <WhatWeDoComponent />
        <SalesDepartmentInfoComponent />
        <CompanyValuesComponent />
        <LatestNewsComponent />
      </div>
    );
  }
}

HomePage.propTypes = {};

export default HomePage;

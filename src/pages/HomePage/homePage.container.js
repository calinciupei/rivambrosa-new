import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import HomePage from './homePage.component';

export default connect()(withTranslation()(HomePage));

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Card, CardActions, CardMedia, CardContent} from '@material-ui/core';
import FacebookIcon from '@material-ui/icons/Facebook';

class UnderConstruction extends Component {
  changeLanguage(locale) {
    const {i18n} = this.props;

    i18n.changeLanguage(locale);
  }

  render() {
    const {t} = this.props;

    return (
      <Grid
        container
        spacing={0}
        alignItems="center"
        justify="center"
        style={{
          textAlign: 'center',
        }}
      >
        <Grid item xs={12} sm={6}>
          <Card style={{boxShadow: 'none'}}>
            <CardMedia
              component="img"
              alt="Rivambrosa under construction"
              image="/images/logo.png"
              style={{
                width: 'auto',
                height: 'auto',
                textAlign: 'center',
                display: 'inline-block',
                padding: '20px',
              }}
            />
            <CardContent>
              <Typography variant="h5" component="h2">
                {t('under_construction')}
              </Typography>
            </CardContent>
            <CardActions
              style={{
                textAlign: 'center',
                display: 'inline-block',
              }}
            >
              <a
                aria-label="facebook"
                justify="center"
                href="https://www.facebook.com/rivambrosasrl/"
                target="_self"
              >
                <FacebookIcon color="primary" fontSize="large" />
              </a>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    );
  }
}

UnderConstruction.propTypes = {
  t: PropTypes.func,
  i18n: PropTypes.object,
};

export default UnderConstruction;

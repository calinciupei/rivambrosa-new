import {withTranslation} from 'react-i18next';
import UnderConstruction from './underConstructionPage.component';

export default withTranslation()(UnderConstruction);

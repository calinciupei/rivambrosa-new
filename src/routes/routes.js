import React, {lazy} from 'react';
import PropTypes from 'prop-types';

const UndeConstructionPage = lazy(() =>
  // eslint-disable-next-line max-len
  import(/* webpackPrefetch: true */ /* webpackChunkName: "under-construction" */ './../pages/UndeConstructionPage/underConstructionPage.container'),
);
const HomePage = lazy(() =>
  import(/* webpackPrefetch: true */ /* webpackChunkName: "home" */ './../pages/HomePage/homePage.container'),
);
const AboutUs = lazy(() =>
  import(/* webpackPrefetch: true */ /* webpackChunkName: "about-us" */ './../pages/AboutUs/aboutUs.component'),
);

const Components = {
  AboutUs,
  HomePage,
  UndeConstructionPage,
};

const AsyncComponent = props => {
  const {componentName} = props;

  const Component = Components[componentName];

  return <Component {...props} />;
};

AsyncComponent.propTypes = {
  componentName: PropTypes.string.isRequired,
};

export default AsyncComponent;

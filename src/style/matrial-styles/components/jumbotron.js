import {container} from '../material.style';

const jumbotronStyle = theme => ({
  jumbotron: {
    ...container,
    marginBottom: '50px',
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    marginTop: 0,
    marginBottom: '12px',
  },
  mainText: {
    color: theme.palette.primary.main,
  },
});

export default jumbotronStyle;

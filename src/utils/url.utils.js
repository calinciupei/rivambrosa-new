const getQueryParameter = (param) => {
  const returnParam = decodeURIComponent(
    window.location.search.replace(new RegExp(`^(?:.*[&\\?]${encodeURIComponent(param)
      .replace(/[\\.\\+\\*]/g, '\\$&')}(?:\\=([^&]*))?)?.*$`, 'i'), '$1'));

  return returnParam ? returnParam : null;
};

const getReturnUrlFromQueryParams = () => {
  let returnURL = null;
  const returnUrlNameList = ['rurl', 'url', 'returnURL'];

  returnUrlNameList.forEach((item) => {
    const querySearch = getQueryParameter(item);
    if (querySearch) {
      returnURL = querySearch;
    }
  });

  return returnURL;
};

export {
  getQueryParameter,
  getReturnUrlFromQueryParams,
};
